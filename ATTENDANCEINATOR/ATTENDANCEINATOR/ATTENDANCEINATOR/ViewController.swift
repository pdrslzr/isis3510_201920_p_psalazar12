//
//  ViewController.swift
//  ATTENDANCEINATOR
//
//  Created by Pedro Salazar on 11/8/19.
//  Copyright © 2019 Pedro Salazar. All rights reserved.
//

import UIKit
import CoreBluetooth



class ViewController: UIViewController {
    
    var peripheralManager: CBPeripheralManager!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        peripheralManager = CBPeripheralManager(delegate: self, queue: nil)
    }
    
    func advertisePlease() {
        peripheralManager.startAdvertising([CBAdvertisementDataServiceUUIDsKey:[0x1101],
                                            CBAdvertisementDataLocalNameKey: "hola"])
    }
  


}

func peripheralManagerDidStartAdvertising(peripheral: CBPeripheralManager, error: NSError?) {
    print("started advertising")
    print(peripheral)
}


extension ViewController: CBPeripheralManagerDelegate {
    
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        switch peripheral.state {
        case .unknown:
            print("peripheralManager.state is .unknown")
        case .resetting:
            print("peripheralManager.state is .resetting")
        case .unsupported:
            print("peripheralManager.state is .unsupported")
        case .unauthorized:
            print("peripheralManager.state is .unauthorized")
        case .poweredOff:
            print("peripheralManager.state is .poweredOff")
        case .poweredOn:
            print("peripheralManager.state is .poweredOn")
            
            
            
            peripheralManager.startAdvertising([CBAdvertisementDataServiceUUIDsKey:[0x1101],
                                                CBAdvertisementDataLocalNameKey: "hola"])
            
            sleep(2)
            
            print(peripheralManager.isAdvertising)

        @unknown default:
            print("peripheralManager.state is something very weird")
        }
    }
    
    
}






